import React from 'react';

const Header = (props) => (
    <div className="header">
        <div className="container">
            <h1 className="header__title">{props.title}</h1>
            {props.subtitle && <h2 className="header__subtitle">{props.subtitle}</h2>}
            <h2 className="header__subtitle">
                See the repository <a
                href="https://gitlab.com/ShakedHazon/LearnReact/tree/master/indecision-app"
                target="_blank">here</a></h2>
        </div>
    </div>
);
Header.defaultProps = {title: 'Indecision'};
export default Header;