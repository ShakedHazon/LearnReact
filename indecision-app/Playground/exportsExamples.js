console.log('utils.js');

// export const square = (x) => x * x;
// export const add = (a, b) => a + b;
// export default (a, b) => a - b;

const square = (x) => x * x;
const add = (a, b) => a + b;
const substract = (a, b) => a - b;

export {square, add, substract as default};

// in app.js:
// import anyName, {square, add} from "./utils";
//
// console.log('running');
// console.log(square(4));
// console.log(add(5, 3));
// // This will be subtract:
// console.log(anyName(10, 2));